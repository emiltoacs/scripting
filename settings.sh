#!/usr/bin/env bash
grep -qE "^ID=nixos" /etc/os-release && [ -z "$IN_NIX_SHELL" ] && (nix develop --command bash -c "$0"; kill $$ && exit)
HWRAMTOTAL="$(grep MemTotal /proc/meminfo | awk '{print $2}')"; [ "$HWRAMTOTAL" -gt 24000000 ] && CACHEPERCENT=30 || CACHEPERCENT=15; CACHEONRAM=$((HWRAMTOTAL * CACHEPERCENT / 100)); CORUID="$(id -u $USER)"; CORGID="$(id -g $USER)"; FL="$PWD/files"
declare -A BLOCK_LEVELS=(['L0']='-B18 -S18' ['L1']='-B20 -S20' ['L2']='-B22 -S22' ['L3']='-B24 -S24' ['L4']='-B26 -S26'); [ -n "$BLOCK" ] && BLOCK_FLAGS=(${BLOCK_LEVELS[$BLOCK]})
mount() { unmount &> /dev/null; [ -d "$FL/groot" ] && [ "$( ls -A "$FL/groot")" ] && echo "Game is already mounted or extracted." && exit; mkdir -p {"$FL/.groot-mnt","$FL/overlay-storage","$FL/.groot-work","$FL/groot"} && dwarfs "$FL/groot.dwarfs" "$FL/.groot-mnt" -o cachesize="$CACHEONRAM"k -o clone_fd -o cache_image && fuse-overlayfs -o squash_to_uid="$CORUID" -o squash_to_gid="$CORGID" -o lowerdir="$FL/.groot-mnt",upperdir="$FL/overlay-storage",workdir="$FL/.groot-work" "$FL/groot" && echo "Mounted game. Extraction not required."; }
unmount() { fuser -k "$FL/.groot-mnt" 2> /dev/null; fusermount3 -u -z "$FL/groot"; fusermount3 -u -z "$FL/.groot-mnt"; echo " Unmounted game."; }
extract() { [ -d "$FL/groot" ] && [ "$( ls -A "$FL/groot")" ] && echo "Game is already mounted or extracted." && exit; mkdir "$FL/groot"; dwarfsextract --stdout-progress -i "$FL/groot.dwarfs" -o "$FL/groot"; }
compress() { [ ! -f "$FL/groot.dwarfs" ] && mkdwarfs -l7 "${BLOCK_FLAGS[@]}" --no-create-timestamp --order=nilsimsa:255:40000:40000 -i "$FL/groot" -o "$FL/groot.dwarfs" && echo "${BLOCK_FLAGS[@]}" > "$FL/block-lvl.txt"; }
check-integrity() { dwarfsck --check-integrity -i "$FL/groot.dwarfs"; }
for i in "$@"; do if type "$i" &> /dev/null; then "$i"; else exit; fi; done
