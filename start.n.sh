#!/usr/bin/env bash
[ "${NIXDEV:=1}" = "0" ] && "NIX mode disabled." || grep -qE "^ID=nixos" /etc/os-release && [ -z "$IN_NIX_SHELL" ] && (nix develop --command bash -c "$0"; kill $$ && exit)
[ ! -x "$(command -v dwarfs)" ] && echo "dwarfs not installed." && exit; [ ! -x "$(command -v bwrap)" ] && echo "bubblewrap not installed." && exit; [ ! -x "$(command -v fuse-overlayfs)" ] && echo "fuse-overlayfs not installed." && exit; cd "$(dirname "$(readlink -f "$0")")" || exit; [ "$EUID" = "0" ] && exit; export LANG=C
export JCD="${XDG_DATA_HOME:-$HOME/.local/share}/jc141"; [ ! -d "$JCD/native" ] && mkdir -p "$JCD/native"
[ ! -e "$JCD/native/.Xauthority" ] && ln "$XAUTHORITY" "$JCD/native" && XAUTHORITY="$HOME/.Xauthority"

# dwarfs
bash "$PWD/settings.sh" mount; zcat "$PWD/logo.txt.gz";

# auto-unmount
[ "${UNMOUNT:=1}" = "0" ] && echo "Game will not unmount automatically." || { function cleanup { cd "$OLDPWD" && bash "$PWD/settings.sh" unmount; }; trap 'cleanup' EXIT INT SIGINT SIGTERM; }

# bwrap
bubblewrap_run () { [ -n "${WAYLAND_DISPLAY}" ] && export wayland_socket="${WAYLAND_DISPLAY}" || export wayland_socket="wayland-0"
[ -z "${XDG_RUNTIME_DIR}" ] && export XDG_RUNTIME_DIR="/run/user/${EUID}"
[ "${BLOCK_NET:=1}" = "0" ] && echo "Network blocking is not enabled due to user input." || UNSHARE="--unshare-net" && echo "Network blocking enabled. Can disable with BLOCK_NET=0.";
for s in /tmp/.X11-unix/*; do VAR+=(--bind-try "${s}" "${s}"); done

bwrap --new-session --bind / / --ro-bind-try "$HOME" "$HOME" --dev-bind /dev /dev --ro-bind-try /sys /sys --proc /proc  \
      --ro-bind-try /mnt /mnt --ro-bind-try /run /run --ro-bind-try /var /var --ro-bind-try /etc /etc \
      --ro-bind-try /tmp/.X11-unix /tmp/.X11-unix --ro-bind-try /opt /opt --bind-try /tmp /tmp \
      --ro-bind-try /usr/lib64 /usr/lib64 --ro-bind-try /usr/lib /usr/lib \
      --bind-try "$JCD"/native ~/ "${VAR[@]}" --bind "$PWD" "$PWD" $UNSHARE "$@"; }

# start
ROOT="$PWD/files/groot"; CMD=( ./"game" "$@" )
echo "Support can be provided on our Matrix channel."
[ "${DBG:=0}" = "1" ] || { echo "Output muted by default. Can unmute with DBG=1." && exec &>/dev/null; }
[ "${ISOLATION:=1}" = "0" ] && echo "Isolation is disabled." && cd "$ROOT" && "${CMD[@]}" || echo "Isolation is enabled. Any files created by the game in home are redirected to ~/.local/share/jc141/native. Can be disabled with ISOLATION=0." && bubblewrap_run --chdir "$ROOT" "${CMD[@]}"
